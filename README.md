# Fundamentals of PHP II

## Objectives:
* File I/O,
* Form Manipulation / Handling,
* Data Structures,
* Object Oriented Programming,
* Exception Handling

### Aim:
* Accept user input, via the Contact Us form, and the Newsletter Subscription form,
* Store the user information in a separate text file,
* Send an acknowledgement mail to the user, with mail containing information provided by the user,
* The information should be retrieved from the file in which it was stored.



#### Visit the site [Here](https://alphastores.herokuapp.com/wk7.php)

DEVELOPED AND DESIGNED BY [Pamela Agbakoba](http://www.agbakobapamela.org)
*CEO*_BooustD_ nationwide.** &copy; 2018