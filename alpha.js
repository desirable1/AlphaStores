 $(document).ready(function(){
      $("#hideBtn").click (function(event){
        event.preventDefault();
        $('#submitForm').hide();
      });

      $("#showBtn").click (function(event){
        event.preventDefault();
        $("#submitForm").show();
      });

       $("input").focus(function(){
        $(this).next("span").show().fadeOut("slow");
    });
       
      $('#submitForm').click(function(){
          var emailAddress = $('#inputEmail').val();
           var fullName = $('#inputName').val();
           var  message = $('inputMessage').val();
           var  movie = $('#inputMovie').val();

           if (emailAddress === "" && emailAddress !== "email") {
            $('#email_error').text("Valid Email is Required");
           } else {
            $('#email_error').text("");
           }

            if (fullName === "" && fullName !== "name") {
            $('#name_error').text("Enter Your Full Name");
           } else {
            $('#name_error').text("");
           }
          if (movie === "" ) {
            $('#movie_error').text("Please enter a Favorite Movie");
          } else {
            $('#name_error').text("");
           }

            if (message === "") {
            $('#message_error').text("Message Should Contain 50 Characters");
           }

           if (emailAddress !== "" && fullName !== "" && message !== "") {
            var formValues = {
              emailAddress: emailAddress,
              fullName: fullName,
              message: message
            };
            console.log(formValues);
               return true;
           }
           return false;
      });
      $("button").click(function(){
        $(".form_wrapper")
            .animate({width: "300px"},5000)
            .animate({height: "300px"},2000)
            .animate({marginLeft: "150px"}, 3000)
    });
        });

 	// Regular map
function regular_map() {
    var var_location = new google.maps.LatLng(40.725118, -73.997699);

    var var_mapoptions = {
        center: var_location,
        zoom: 14
    };

    var var_map = new google.maps.Map(document.getElementById("map-container-8"),
        var_mapoptions);

    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "New York"
    });
}

google.maps.event.addDomListener(window, 'load', regular_map);